﻿using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CIDRParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var expandedIPs = new List<IPNetwork>();
            string res = String.Empty;
            string res2 = String.Empty;
            List<string> lines = System.IO.File.ReadLines(@"E:\office\pnu\assets\network\data networks for traffic analysis 20200406.csv").ToList();
            lines.ForEach(_ =>
            {
                var textLine = _.Replace(" ", String.Empty);
                var buildingNo = textLine.Split(',')[0].Trim();
                var networkRaw = textLine.Split(',')[1].Trim();

                //var raw = _.Replace(" ", String.Empty);

                var cidr = Byte.Parse(networkRaw.Split('/')[1].Trim()); //Byte.Parse("22");  
                var network = networkRaw.Split('/')[0].Trim();
                IPNetwork ipnetwork = IPNetwork.Parse(network, cidr);
                expandedIPs.Add(ipnetwork);

                //res = String.Format(" or sourceip between [dbo].[NetFlowIPAddressToInt]('{0}') and [dbo].[NetFlowIPAddressToInt]('{1}') \r\n ", ipnetwork.FirstUsable, ipnetwork.LastUsable);
                //res = String.Format("'{0}','{1}', '{2}' \r\n", raw,  ipnetwork.FirstUsable, ipnetwork.LastUsable);
                //res = String.Format("INSERT INTO [dbo].[_PNUDataNetworks]([Network] ,[NetworkStart] ,[NetworkEnd] ,NetworkType)  VALUES ('{0}','{1}','{2}','wireless') \r\n ", raw, ipnetwork.FirstUsable, ipnetwork.LastUsable);
                res = String.Format("<AddressGroup enabled=\"true\" description=\"{0} - {1}\">  \r\n ", buildingNo, networkRaw);
                res += String.Format("\t<Range from=\"{0}\" to=\"{1}\" /> \r\n ", ipnetwork.FirstUsable, ipnetwork.LastUsable);
                res += String.Format("</AddressGroup> \r\n ");

                Console.WriteLine(res);

                res2 += res;
            }
            );



            //using (ExcelEngine excelEngine = new ExcelEngine())
            //{
            //    //https://help.syncfusion.com/file-formats/xlsio/working-with-data
            //    //

            //    IApplication application = excelEngine.Excel;
            //    application.DefaultVersion = ExcelVersion.Excel2013;
            //    IWorkbook workbook = application.Workbooks.Open(@"data networks for traffic analysis.txt");

            //    IWorksheet worksheet = workbook.Worksheets[0];

            //    //Import the data to worksheet
            //    worksheet.ImportData(expandedIPs, 2, 1, false);
            //    workbook.SaveAs("data networks for traffic analysis - expanded IPs.xlsx");
            //    workbook.Close();

            //}
        }
    }

}
