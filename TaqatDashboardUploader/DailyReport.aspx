﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DailyReport.aspx.cs" Inherits="TaqatDashboardUploader.DailyReport" %>
<%@ Register assembly="Syncfusion.EJ.Web, Version=16.2460.0.41, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89" namespace="Syncfusion.JavaScript.Web" tagprefix="ej" %>
<%@ Register assembly="Syncfusion.EJ, Version=16.2460.0.41, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89" namespace="Syncfusion.JavaScript.Models" tagprefix="ej" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width:100%;">
        <tr>
            <td style="width:40%"><h2>TAQAT Daily Report</h2></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Report Date"></asp:Label>
            </td>
            <td>
                <ej:DatePicker ID="txtReportDate" runat="server" DateFormat="">
                </ej:DatePicker>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="No Of Logins (Individuals)"></asp:Label>
            </td>
            <td>
                
                <ej:NumericTextBox ID="txtNoOfLoginsIndividuals" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
                
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="No Of Logins (Employers)"></asp:Label>
            </td>
            <td>
                <ej:NumericTextBox ID="txtNoOfLoginsEmployers" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="No Of Registrations (Individuals)"></asp:Label>
            </td>
            <td>
                <ej:NumericTextBox ID="txtNoOfRegistrationsIndividuals" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="No Of Registrations (Employers)"></asp:Label>
            </td>
            <td>
                <ej:NumericTextBox ID="txtNoOfRegistrationsEmployers" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 22px"></td>
            <td style="height: 22px"></td>
            <td style="height: 22px"></td>
        </tr>
        <tr>
            <td style="height: 32px">
                <asp:Label ID="Label5" runat="server" Text="No Of Jobs Posted"></asp:Label>
            </td>
            <td style="height: 32px">
                <ej:NumericTextBox ID="txtNoOfJobsPosted" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
            </td>
            <td style="height: 32px"></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="No Of Applications Submitted"></asp:Label>
            </td>
            <td>
                <ej:NumericTextBox ID="txtNoOfApplicationsPosted" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="No Of Max Connections"></asp:Label>
            </td>
            <td>
                <ej:NumericTextBox ID="txtNoOfMaxConnections" runat="server" ShowSpinButton="False">
                </ej:NumericTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="cmdSendReport" runat="server" OnClick="cmdSendReport_Click" Text="Send Report" />
            </td>
        </tr>
    </table>
</asp:Content>
