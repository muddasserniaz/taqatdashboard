﻿using CsvHelper;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Configuration;
using TaqatDashboardUploader.Properties;

namespace TaqatDashboardUploader
{

    public class Emailer
    {

        public void SendEmail(string subject, string body, string attachmentPath, string emailAddressesFile)
        {
            //string senderName = "ops@taqat.sa";
            //string mailServer = "10.30.60.70";
            //int mailServerPort = 25;
            //string senderEmailId = "ops@taqat.sa";

            string senderName = WebConfigurationManager.AppSettings["senderName"];
            string mailServer = WebConfigurationManager.AppSettings["mailServer"];
            int mailServerPort = int.Parse(WebConfigurationManager.AppSettings["mailServerPort"]);
            string senderEmailId = WebConfigurationManager.AppSettings["senderEmailId"];

            //string senderName = "Muhammad Muddasser Niaz";
            //string mailServer = "127.0.0.1";
            //int mailServerPort = 2525;
            //string senderEmailId = "niazmm@aecl.com";

            var fromAddress = new MailAddress(senderEmailId, senderName);

            var smtp = new SmtpClient
            {
                Host = mailServer,
                Port = mailServerPort,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                //Credentials = new NetworkCredential(fromAddress.Address, password)
            };
            using (var message = new MailMessage()
            {
                Subject = subject,
                Body = body
            })
            {
                var data = new System.Net.Mail.Attachment(
                                        attachmentPath,
                                         MediaTypeNames.Application.Octet);

                message.Attachments.Add(data);

                message.From = fromAddress;
                ReadEmailAddressesFromFile(emailAddressesFile, message);

                smtp.Send(message);
            }
        }

        private MailMessage ReadEmailAddressesFromFile(string emailAddressesFilePath, MailMessage message)
        {
            using (TextReader fileReader = File.OpenText(emailAddressesFilePath))
            {
                var csv = new CsvReader(fileReader);
                var emailAddresses = csv.GetRecords<dynamic>().ToList();
                foreach (var emailaddress in emailAddresses)
                {
                    if (emailaddress.EmailType == "TO")
                        message.To.Add(emailaddress.EmailAddress);
                    else if (emailaddress.EmailType == "CC")
                        message.CC.Add(emailaddress.EmailAddress);
                    else if (emailaddress.EmailType == "BCC")
                        message.Bcc.Add(emailaddress.EmailAddress);
                }
            }
            return message;
        }

        public void SendMailEWS(string subject, string body, string attachmentPath, string emailAddressesFile)
        {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
            service.TraceEnabled = true;
            service.TraceFlags = TraceFlags.EwsResponse;
            service.Credentials = new WebCredentials(Settings.Default.EWSDefaultUserName, Settings.Default.EWSDefaultPassword);
            service.UseDefaultCredentials = false;
            service.Url = new Uri("https://mymail.aecl.com/EWS/Exchange.asmx");
            //service.AutodiscoverUrl("niazmm@aecl.com", RedirectionUrlValidationCallback);
            EmailMessage email = new EmailMessage(service);

            email.Subject = subject;
            email.Body = new MessageBody(body);
            ReadEmailAddressesFromFileForEWS(emailAddressesFile, email);
            email.Attachments.AddFileAttachment(attachmentPath);

            email.Send();
        }

        private EmailMessage ReadEmailAddressesFromFileForEWS(string emailAddressesFilePath, EmailMessage message)
        {
            using (TextReader fileReader = File.OpenText(emailAddressesFilePath))
            {
                var csv = new CsvReader(fileReader);
                var emailAddresses = csv.GetRecords<dynamic>().ToList();
                foreach (var emailaddress in emailAddresses)
                {
                    if (emailaddress.EmailType == "TO")
                        message.ToRecipients.Add(emailaddress.EmailAddress);
                    else if (emailaddress.EmailType == "CC")
                        message.CcRecipients.Add(emailaddress.EmailAddress);
                    else if (emailaddress.EmailType == "BCC")
                        message.BccRecipients.Add(emailaddress.EmailAddress);
                }
            }
            return message;
        }

    }


}

