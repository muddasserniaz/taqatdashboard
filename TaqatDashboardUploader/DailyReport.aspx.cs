﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaqatDashboardUploader
{

    public partial class DailyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtReportDate.Value = DateTime.Now.AddDays(-1);
            }
        }

        protected void cmdSendReport_Click(object sender, EventArgs e)
        {

            var report = PrepareReport();
            SendEmail(report);

        }

        private string PrepareReport()
        {

            var dreport = new TaqatDashboard.TaqatDashboardPptWriter();
            var pdfResult = dreport.PrepareDailyReport(
                string.Format("{0:dd MMM yy}", txtReportDate.Value),
                txtNoOfLoginsIndividuals.Value,
                txtNoOfLoginsEmployers.Value,
                txtNoOfRegistrationsIndividuals.Value,
                txtNoOfRegistrationsEmployers.Value,
                txtNoOfJobsPosted.Value,
                txtNoOfApplicationsPosted.Value,
                txtNoOfMaxConnections.Value);

            return pdfResult;
        }

        private void SendEmail(string filePath)
        {
            var t = new Emailer();
            string subject = string.Format("Daily Activity on TAQAT {0}", string.Format("{0:dd MMM yy}", txtReportDate.Value));
            string body = "Please find attached the subjected report.";

            t.SendMailEWS(subject, body, filePath, @"c:\taqatdashboard\inputs\EmailAddresses_DailyReport_EWS.csv");
        }


    }

}