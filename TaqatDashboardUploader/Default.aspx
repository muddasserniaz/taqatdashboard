﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TaqatDashboardUploader._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .comboWidth {
            max-width: 100%;
        }
    </style>

            <table style="width:100%">
                <tr>
                    <td colspan="5" style="height: 65px" >
                        <h2>TAQAT Dashboard - Hourly Report</h2>
                        <h3>Server Time : <%=DateTime.Now.ToString("dd-MMM-yy hh:mm tt") %></h3>
                    </td>
                </tr>
                <tr>
                    <td style="height: 44px">
                        Select File To Upload</td>
                    <td style="height: 44px">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                       </td>
                    <td style="height: 44px">
                        &nbsp;</td>
                    <td style="height: 44px">
                        &nbsp;</td>
                    <td style="text-align:right; height: 44px;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Speed Test</td>
                    <td>
                        <asp:TextBox ID="txtSpeedTest" runat="server"></asp:TextBox>
                       </td>
                    <td>
                        &nbsp;&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Enter link speed in Mbps e.g. 10</td>
                    <td>
                        &nbsp;&nbsp;</td>
                    <td>
                        &nbsp;&nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Label ID="Label1" runat="server" style="color: #FF0000">Result: </asp:Label>
                    </td>
                    <td>
                        &nbsp;&nbsp;</td>
                    <td>
                        &nbsp;&nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="cmdUploadAndSend" runat="server" Text="Upload and Send Report" OnClick="cmdUploadAndSend_Click" />

                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:CheckBox ID="chkSendEmail" runat="server" Checked="True" Text="Send Email" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:DropDownList ID="cmbFileList" CssClass="comboWidth" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbFileList_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Download" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:ListBox ID="lstEmails" runat="server" Height="400px" Width="259px"></asp:ListBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td style="vertical-align:top">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="500px">
                            <Columns>
                                <asp:BoundField DataField="MetricName" HeaderText="MetricName" />
                                <asp:BoundField DataField="PageLoadtime" HeaderText="PageLoadtime" />
                                <asp:BoundField DataField="PageLoadtimeOriginal" HeaderText="Org" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="cmdSendDirectly" runat="server" OnClick="cmdSendDirectly_Click" Text="Send Directly" />
                    &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>




</asp:Content>
