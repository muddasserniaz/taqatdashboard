﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TaqatDashboard;

namespace TaqatDashboardUploader
{
    public partial class _Default : Page
    {
        string uploadPath = @"c:\taqatdashboard\uploads\";
        string outputsPath = @"c:\taqatdashboard\outputs\";
        static List<String> toEmailAddresses;

        private void PrepareEmailAddresses()
        {
            toEmailAddresses = new List<string>();

            //toEmailAddresses.Add("BaderS@aecl.com");
            //toEmailAddresses.Add("a.alghofaily@hrdf.org.sa");
            //toEmailAddresses.Add("MAKHDOUBH@aecl.com");
            //toEmailAddresses.Add("AhmedN@aecl.com");
            //toEmailAddresses.Add("KhokharFR@aecl.com");
            //toEmailAddresses.Add("a.farah-c@hrdf.org.sa");
            //toEmailAddresses.Add("a.alnuhait-c@hrdf.org.sa");
            //toEmailAddresses.Add("A.alharbi-c@hrdf.org.sa");
            //toEmailAddresses.Add("a.aldarrak@hrdf.org.sa");
            //toEmailAddresses.Add("a.alguwyeri@hrdf.org.sa");
            //toEmailAddresses.Add("A.Alhomali@hrdf.org.sa");
            //toEmailAddresses.Add("n.alrasheed@hrdf.org.sa");
            //toEmailAddresses.Add("S-Jumaiah@hrdf.org.sa");
            //toEmailAddresses.Add("M.Alkahtani@hrdf.org.sa");
            //toEmailAddresses.Add("AliSD@aecl.com");

            foreach (var toEmail in toEmailAddresses)
            {
                lstEmails.Items.Add(toEmail);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            HideAdminControls();
            if (!IsPostBack)
            {
                PrepareEmailAddresses();
                var sortedFiles = new DirectoryInfo(outputsPath).GetFiles("*.csv")
                                                  .OrderByDescending(f => f.CreationTime)
                                                  .Take(20)
                                                  .Where(_ => !_.Name.Contains("_ORG"))
                                                  .Where(_ => !_.Name.Contains("FAIL"))
                                                  .ToList();


                foreach (var f in sortedFiles)
                {
                    cmbFileList.Items.Add(new ListItem() { Text = f.Name });
                }
                //refresh to show dile data
                cmbFileList_SelectedIndexChanged(cmbFileList, null);
            }
        }
        private void HideAdminControls()
        {
            if (Request.QueryString["Admin"] == "dsvfege3rt3tfegfd")
            {
                Label1.Text += "admin";
                cmbFileList.Visible = true;
                GridView1.Visible = true;
                cmdSendDirectly.Visible = true;
                Button1.Visible = true;
                chkSendEmail.Visible = true;
            }
            else
            {
                //Label1.Text += "-select file to upload\r\n-enter speed test in textbox\r\n-press upload and send to send report\r\n-save pdf locally for reference";

                cmbFileList.Visible = false;
                GridView1.Visible = false;
                cmdSendDirectly.Visible = false;
                Button1.Visible = false;
                chkSendEmail.Visible = false;
            }
        }
        protected void cmdTestEmail_Click(object sender, EventArgs e)
        {
            // SendMail();
        }
        //public void SendMail_TBD(string filePath)
        //{
        //    try
        //    {
        //        // string receiverEmailId = "muddasserniaz@gmail.com";
        //        string senderName = "ops@taqat.sa";
        //        //string mailServer = "10.48.41.161"; ;
        //        string mailServer = "10.30.60.70"; ;
        //        string senderEmailId = "ops@taqat.sa";
        //        //string password = ConfigurationManager.AppSettings["SMTPPasssword"].ToString();
        //        var fromAddress = new MailAddress(senderEmailId, senderName);
        //        // var toAddress = new MailAddress(receiverEmailId, "muddasserniaz@gmail.com");
        //        string subject = "TAQAT Dashboard " + DateTime.Now.ToString("dd-MMM hh tt");
        //        string body = "Please find attached the subjected report.";
        //        var smtp = new SmtpClient
        //        {
        //            Host = mailServer,
        //            Port = 25,
        //            EnableSsl = false,
        //            DeliveryMethod = SmtpDeliveryMethod.Network,
        //            //Credentials = new NetworkCredential(fromAddress.Address, password)
        //        };
        //        using (var message = new MailMessage()
        //        {
        //            Subject = subject,
        //            Body = body
        //        })
        //        {
        //            // create attachment and set media Type
        //            //      see http://msdn.microsoft.com/de-de/library/system.net.mime.mediatypenames.application.aspx
        //            Attachment data = new Attachment(
        //                                    filePath,
        //                                     MediaTypeNames.Application.Octet);
        //            // your path may look like Server.MapPath("~/file.ABC")
        //            message.Attachments.Add(data);

        //            message.From = fromAddress;

        //            //add all to email addresses
        //            foreach (var toEmail in toEmailAddresses)
        //            {
        //                message.To.Add(toEmail);
        //            }

        //            message.CC.Add("ShareefMS@aecl.com");

        //            message.CC.Add("SiddiqueA@aecl.com");
        //            message.CC.Add("AbdulrafeeqM@aecl.com");
        //            //message.CC.Add("RashidJ@aecl.com");
        //            message.CC.Add("niazmm@aecl.com");
        //            message.Bcc.Add("muddasserniaz@gmail.com");
        //            message.Bcc.Add("aboul.hassan.siddique@gmail.com");
        //            message.Bcc.Add("shareef.sanaullah@gmail.com");

        //            smtp.Send(message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Label1.Text += ex.Message;
        //    }
        //}
        private void SendProcessedFileToUploader(string filePath)
        {
            Response.ContentType = "Application/pdf";
            Response.AppendHeader("content-disposition",
                    "attachment; filename=" + filePath);
            Response.TransmitFile(filePath);
            Response.End();
        }
        protected void cmdUploadAndSend_Click(object sender, EventArgs e)
        {
            //cmdUploadAndSend.Enabled = false;
            if (txtSpeedTest.Text == "")
            {
                Label1.Text += "Please enter link speed";
            }
            else
            {

                if (IsPostBack)
                {
                    Boolean fileOK = false;
                    //String path = Server.MapPath("~/UploadedCsv/");

                    if (FileUpload1.HasFile)
                    {
                        String fileExtension =
                            System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                        String[] allowedExtensions =
                            {".csv"};
                        for (int i = 0; i < allowedExtensions.Length; i++)
                        {
                            if (fileExtension == allowedExtensions[i])
                            {
                                fileOK = true;
                            }
                        }
                    }

                    if (fileOK)
                    {
                        try
                        {
                            //FileUpload1.PostedFile.ContentType
                            var fileNameWithPath = uploadPath + FileUpload1.FileName;
                            FileUpload1.PostedFile.SaveAs(fileNameWithPath);
                            Label1.Text = "-File uploaded!";

                            //send report using uploaded file.
                            SendReport(fileNameWithPath);

                            ////FileUpload1.PostedFile.ContentType
                            //FileUpload1.PostedFile.SaveAs(uploadPath + FileUpload1.FileName);
                            //Label1.Text = "-File uploaded!";
                            //var x = new TaqatDashboard.TaqatDashboardPptWriter();
                            //var pdfFilePath = x.ConvertCSVToReportAndSend(uploadPath + FileUpload1.FileName, DateTime.Now.ToString("dd MMM yyyy - hh tt"), txtSpeedTest.Text);
                            //SendMail(pdfFilePath);
                            //Label1.Text += "\r\n-processed and email sent";
                            //SendProcessedFileToUploader(pdfFilePath);
                        }
                        catch (Exception ex)
                        {
                            Label1.Text += string.Format("\r\nERROR:  {0}", ex.Message);
                        }

                        //delete uploaded file
                        System.IO.File.Delete(uploadPath + FileUpload1.FileName);
                    }
                    else
                    {
                        Label1.Text += "\r\nCannot accept files of this type.";
                    }


                }
            }
        }

        private void SendReport(string fileNameWithPath, bool useEWS = false)
        {
            try
            {

                var x = new TaqatDashboard.TaqatDashboardPptWriter();
                var pdfFilePath = x.ConvertCSVToReportAndSend(fileNameWithPath, DateTime.Now.ToString("dd MMM yyyy - hh tt"), txtSpeedTest.Text);

                //send email
                var newMailer = new Emailer();
                if (useEWS) //sending as niazmm
                {
                    if (chkSendEmail.Checked)
                    {
                        newMailer.SendMailEWS(
                            subject: "TAQAT Dashboard " + DateTime.Now.ToString("dd-MMM hh tt"),
                            body: "Please find attached the subjected report.",
                            attachmentPath: pdfFilePath,
                            emailAddressesFile: @"c:\taqatdashboard\inputs\EmailAddresses_HourlyReport_EWS.csv");
                    }
                }
                else
                {
                    //SendMail(pdfFilePath);
                    newMailer.SendEmail( //sending as ops@taqat.sa
                       subject: "TAQAT Dashboard " + DateTime.Now.ToString("dd-MMM hh tt"),
                       body: "Please find attached the subjected report.",
                       attachmentPath: pdfFilePath,
                       emailAddressesFile: @"c:\taqatdashboard\inputs\EmailAddresses_HourlyReport.csv");
                }

                Label1.Text += "\r\n-processed and email sent";
                SendProcessedFileToUploader(pdfFilePath);
            }
            catch (Exception ex)
            {
                Label1.Text += string.Format("\r\nERROR:  {0}", ex.Message);
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var filenamewithpath = outputsPath + cmbFileList.Text;
            //string pdfPath = MapPath("mypdf.pdf");
            Response.ContentType = "text/csv";
            Response.AppendHeader("content-disposition",
                    "attachment; filename=" + filenamewithpath);
            Response.TransmitFile(filenamewithpath);
            Response.End();
        }

        protected void cmbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("MetricName", typeof(string));
            dt.Columns.Add("PageLoadTime", typeof(double));
            dt.Columns.Add("PageLoadTimeOriginal", typeof(double));

            var x = new TaqatDashboard.TaqatDashboardPptWriter();
            var resultList = x.ReadInCSV(outputsPath + cmbFileList.Text);
            //irg exists then use it compare
            var orgFilePath = outputsPath + cmbFileList.Text.Replace("_Source", "_ORG_Source");
            List<TaqatDashboardResult> resultList_org = null;
            if (System.IO.File.Exists(orgFilePath))
            {
                var x_org = new TaqatDashboard.TaqatDashboardPptWriter();
                resultList_org = x.ReadInCSV(orgFilePath);
            }
            foreach (var result in resultList)
            {
                DataRow NewRow = dt.NewRow();
                NewRow[0] = result.MetricName;
                NewRow[1] = Math.Round(result.PageLoadTime, 1);
                if (System.IO.File.Exists(orgFilePath))
                {
                    NewRow[2] = Math.Round((resultList_org.FirstOrDefault(_ => _.MetricName == result.MetricName).PageLoadTime / 1000), 1); //original val

                }
                dt.Rows.Add(NewRow);
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void cmdSendDirectly_Click(object sender, EventArgs e)
        {

            //send report from selected file in combo list
            SendReport(outputsPath + cmbFileList.Text, true);
        }


    }
}