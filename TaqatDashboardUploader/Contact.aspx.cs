﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaqatDashboardUploader
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Data"]!= null)
            {
                var str = string.Format(@"{0} - {1}", DateTime.Now.ToString(), Request.QueryString["Data"].ToString());
                System.IO.File.AppendAllText(@"c:\taqatdashboard\a.txt", str+System.Environment.NewLine);
            }
        }

    }
}