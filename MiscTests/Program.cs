﻿using Microsoft.Exchange.WebServices.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace MiscTests
{
    class Program
    {
        static void Main(string[] args)
        {
            //EAArchitectTest.EAArchitectConnectTest();
            EWSManagedApi();
            //AsposeEmailTest.Run();

            //new GoogleAnalyticsTest().Test();
            //new GoogleAnalyticsTest().TestRealtime();
            //GoogleAnalyticsTestWithSelenium();
            //SharexStartRecodring();
            //ProcessNameTest();
            //int lastTier = 999999;

            //lastTier = DeltaAlertTest(lastTier, 4300);
            //lastTier = DeltaAlertTest(lastTier, 5340);
            //lastTier = DeltaAlertTest(lastTier, 4111);
            //lastTier = DeltaAlertTest(lastTier, 6330);
            //lastTier = DeltaAlertTest(lastTier, 4001);
            //lastTier = DeltaAlertTest(lastTier, 1001);
            //lastTier = DeltaAlertTest(lastTier, 430);
            //lastTier = DeltaAlertTest(lastTier, 0);
            //lastTier = DeltaAlertTest(lastTier, 1);
            //lastTier = DeltaAlertTest(lastTier, 10);
            //lastTier = DeltaAlertTest(lastTier, 100);
            //lastTier = DeltaAlertTest(lastTier, 1001);
            //lastTier = DeltaAlertTest(lastTier, 9000);
            //lastTier = DeltaAlertTest(lastTier, 21500);
            //lastTier = DeltaAlertTest(lastTier, 6040);
            //lastTier = DeltaAlertTest(lastTier, 9394);

            //Console.WriteLine("{0}", );
            Console.ReadKey();
        }

        public static int RoundOff(int i)
        {
            return ((int)Math.Round(i / 1000.0)) * 1000;
        }

        private static int DeltaAlertTest(int lastTier, int rawVal)
        {
            //var lastTier  = 0;
            //var currentVal = 4500;
            //var lastVal = 0;
            var currentVal = RoundOff(rawVal);
            var currentTier = currentVal/1000;

            //var currentTier = -1;
            //switch (currentVal)
            //{
            //    case int n when (n > 4000 && n <= 5000):
            //        currentTier = 4;
            //        break;

            //    case int n when (n > 5000 && n <= 6000):
            //        currentTier = 5;
            //        break;

            //    case int n when (n > 6000 && n <= 7000):
            //        currentTier = 6;
            //        break;

            //    default:
            //        break;
            //}
            //tier changed and went up 
            if (currentTier != lastTier && currentTier > lastTier)
            {
                Console.WriteLine("alert {0}-{1}-{2}-{3}", lastTier, currentTier, rawVal, currentVal);
            }
            else
            {
                Console.WriteLine("no alert {0}-{1}-{2}-{3}", lastTier, currentTier, rawVal, currentVal);
            }
            return currentTier;
        }


        static public void ProcessNameTest()
        {
            foreach (var dp in Process.GetProcesses())
            {
                Console.WriteLine("{0}", dp.ProcessName);
            }

            var p = Process.GetProcessesByName("sharex").FirstOrDefault();

            if (Process.GetProcessesByName("sharex").Any())
            {
                p.Exited += ProcessTerminated;
                //p.Close();
                p.Kill();
                Console.WriteLine("recorder still running.. trying again to close the process");
            }
        }

        static void ProcessTerminated(Object sender, EventArgs e)
        {
            Console.WriteLine("process now exited..");
        }

        static public void SharexStartRecodring()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\Program Files\Sharex\Sharex.exe";
            startInfo.Arguments = @"-StartScreenRecorder -AutoClose";
            process.StartInfo = startInfo;
            process.Start(); //start recording

            Thread.Sleep(10000);

            process.Start(); //call again to stop recording 

            process.Close();
        }

        static public void GoogleAnalyticsTestWithSelenium()
        {
            RemoteWebDriver driver;
            //ChromeOptions chromeDriverOptions;
            //chromeDriverOptions = new ChromeOptions();
            //chromeDriverOptions.AddArguments("disable-infobars");
            //driver = new ChromeDriver(ChromeDriverService.CreateDefaultService(), chromeDriverOptions, TimeSpan.FromSeconds(180));
            driver = new FirefoxDriver();

            driver.Navigate().GoToUrl("https://analytics.google.com");

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));

            var loginTextBox = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("input[autocomplete=username]")));
            loginTextBox.SendKeys("nlg.taqat.monitoring@gmail.com");
            var nextLoginScreen = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#identifierNext")));
            nextLoginScreen.Click();
            var passwordTextBox = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("input[name='password']")));
            passwordTextBox.SendKeys("7sBO8MJW");

            var nextPasswordScreen = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#passwordNext")));
            nextPasswordScreen.Click();

            driver.Navigate().GoToUrl("https://analytics.google.com/analytics/web/?authuser=1#/realtime/rt-overview/a104543447w156164592p157756982/");
            wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#galaxyIframe")));
            driver.SwitchTo().Frame(driver.FindElement(By.Id("galaxyIframe")));

            for (int i = 0; i < 60; i++)
            {
                var activeUsersCountDiv = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div#ID-overviewCounterValue")));
                Console.WriteLine("user: {0}", activeUsersCountDiv.Text);
                Thread.Sleep(60000);
            }

            driver.Close();

        }

        public static void EWSManagedApi()
        {
                   
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
         
            service.TraceEnabled = true;
            service.TraceFlags = TraceFlags.All;
            service.Credentials = new WebCredentials("mmniaz@pnu.edu.sa", "aec@aec123");
            service.UseDefaultCredentials = false;
            service.Url = new Uri("https://mail.pnu.edu.sa/EWS/Exchange.asmx");
            //service.AutodiscoverUrl("niazmm@aecl.com", RedirectionUrlValidationCallback);
            EmailMessage email = new EmailMessage(service);

            email.ToRecipients.Add("muddasserniaz@gmail.com");

            email.Subject = "test x";
            email.Body = new MessageBody("test x");

            //email.Attachments.AddFileAttachment(@"C:\taqatdashboard\outputs\asd.pdf");

            //email.Attachments[0].ContentType = MediaTypeNames.Application.Pdf;
            //////email.Attachments[0].Name = "";

            //byte[] theBytes = File.ReadAllBytes(@"C:\taqatdashboard\outputs\n.zip");
            //// The byte array file attachment is named ThirdAttachment.jpg.
            //email.Attachments.AddFileAttachment("n.zip", theBytes);

            email.Send();

            Console.WriteLine("-------------------");
            Console.ReadLine();

        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }
    }
}
