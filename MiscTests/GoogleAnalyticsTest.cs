﻿using CsvHelper;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MiscTests
{
    public class GoogleAnalyticsTest
    {
        public void Test()
        {
            try
            {
                var credential = GetCredential().Result;
                using (var svc = new AnalyticsReportingService(
                    new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "Google Analytics API Console"
                    }))
                {
                    var dateRange = new DateRange
                    {
                        StartDate = "2018-08-12",
                        EndDate = "2018-08-19"
                    };
                    var sessions = new Metric
                    {
                        Expression = "ga:sessions",
                        Alias = "Sessions"
                    };
                    var date = new Dimension { Name = "ga:date" };

                    var reportRequest = new ReportRequest
                    {
                        DateRanges = new List<DateRange> { dateRange },
                        Dimensions = new List<Dimension> { date },
                        Metrics = new List<Metric> { sessions },
                        ViewId = "157756982"
                    };
                    var getReportsRequest = new GetReportsRequest
                    {
                        ReportRequests = new List<ReportRequest> { reportRequest }
                    };
                    var batchRequest = svc.Reports.BatchGet(getReportsRequest);
                    var response = batchRequest.Execute();
                    foreach (var x in response.Reports.First().Data.Rows)
                    {
                        Console.WriteLine(string.Join(", ", x.Dimensions) +
                        "   " + string.Join(", ", x.Metrics.First().Values));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }

        public void TestRealtime()
        {
            string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data

            //var keyFilePath = @"c:\file.p12";    // Downloaded from https://console.developers.google.com
            //var serviceAccountEmail = "xx@developer.gserviceaccount.com";  // found https://console.developers.google.com

            //loading the Key file
            //var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            //var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            //{
            //    Scopes = scopes
            //}.FromCertificate(certificate));

            var credential = GetCredential().Result;
            var service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Google Analytics API Console",
            });

            //AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
            //{
            //    ApiKey = "[API key]",  // from https://console.developers.google.com (Public API access)
            //    ApplicationName = "Analytics API Sample",
            //});

            var profileId = "157756982";// all public
            //var profileId = "157744661";// individual
            while (true)
            {
                DataResource.RealtimeResource.GetRequest request = service.Data.Realtime.Get(String.Format("ga:{0}", profileId), "rt:activeUsers");
                RealtimeData feed = request.Execute();

                using (TextWriter writer = new StreamWriter(String.Format(@"Taqat_GoogleAnalytics_Realtime_stats.csv"), 
                    append: true))
                {
                    using (var csv = new CsvWriter(writer))
                    {
                        //csv.WriteRecords(feed.Rows);
                        Console.WriteLine(string.Format("{3}, {2}, {0}, {1}",
                            DateTime.Now.ToString("yyyy-M-dd HH:mm:ss"),
                            feed.Rows[0][0].ToString(), "rt:activeUsers", "AllPublic-157756982"));

                        csv.WriteRecord(new
                        {
                            ViewProfileId = "AllPublic-157756982",
                            Metric = "rt:activeUsers",
                            DateTime = DateTime.Now.ToString("yyyy-M-dd HH:mm:ss"),
                            MetricValue = feed.Rows[0][0]
                        });
                        writer.WriteLine();
                        writer.Flush();
                    }
                }

                //foreach (IList<string> row in feed.Rows)
                //{
                //    foreach (string col in row)
                //    {
                //        Console.Write(col + " ");  // writes the value of the column
                //    }
                //    Console.Write("\r\n");
                //}
                //foreach (var headers in feed.ColumnHeaders)
                //{
                //    Console.WriteLine(String.Format("{0} - {1} - {2}", headers.Name, headers.ColumnType, headers.DataType));
                //}

                Thread.Sleep(15000);
            }

            //Console.WriteLine("press any key...");
            //Console.ReadLine();
        }

        static async Task<UserCredential> GetCredential()
        {
            using (var stream = new FileStream("client_secret.json",
                 FileMode.Open, FileAccess.Read))
            {
                const string loginEmailAddress = "nlg.taqat.monitoring@gmail.com";
                return await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { AnalyticsReportingService.Scope.Analytics },
                    loginEmailAddress, CancellationToken.None,
                    new FileDataStore("GoogleAnalyticsApiConsole"));
            }
        }
    }
}
