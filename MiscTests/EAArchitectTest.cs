﻿using EASendMail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiscTests
{
    public class EAArchitectTest
    {
        public static void EAArchitectConnectTest()
        {
            SmtpMail oMail = new SmtpMail("TryIt");
            SmtpClient oSmtp = new SmtpClient();

            // Set sender email address, please change it to yours
            oMail.From = "niazmm@aecl.com";
            // Set recipient email address, please change it to yours
            oMail.To = "muddasserniaz@gmail.com";

            // Set email subject
            oMail.Subject = "testx2";

            // Set email body
            oMail.TextBody = "testx2";

            // Your Exchange Server address
            //SmtpServer oServer = new SmtpServer("https://mymail.aecl.com/EWS/Exchange.asmx");
            SmtpServer oServer = new SmtpServer("mymail.aecl.com");

            // Set Exchange Web Service EWS - Exchange 2007/2010/2013/2016
            oServer.Protocol = ServerProtocol.ExchangeEWS;
            // User and password for Exchange authentication
            oServer.User = "niazmm@aecl.com";
            oServer.Password = "AbCd1234567";

            // By default, Exchange Web Service requires SSL connection
            oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

            try
            {
                Console.WriteLine("start to send email ...");
                oSmtp.SendMail(oServer, oMail);
                Console.WriteLine("email was sent successfully!");
            }
            catch (Exception ep)
            {
                Console.WriteLine("failed to send email with the following error:");
                Console.WriteLine(ep.Message);
                Console.ReadLine();
            }
        }

    }
    
}
