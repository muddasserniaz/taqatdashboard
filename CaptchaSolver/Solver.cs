﻿using CaptchaSolver.Api;
using CaptchaSolver.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaptchaSolver
{
    public class Solver
    {

        public class SolverResult
        {
            public string Result { get; set; }
            public bool Success { get; set; }
        }

        public static SolverResult GetBalance()
        {

            var res = new SolverResult();
            var api = new ImageToText
            {
                ClientKey = "b8412fbb606dda05faf628e7c43df66f"
            };

            var balance = api.GetBalance();
            if (balance == null)
            {
                res.Result = "GetBalance() failed. " + api.ErrorMessage;
                res.Success = false;
            }
            else
            {
                res.Result = balance.ToString();
                res.Success = true;
            }

            return res;
        }

        public static SolverResult SolveImageToText(string filePath = "")
        {
            var res = new SolverResult();


            var api = new ImageToText
            {
                ClientKey = "b8412fbb606dda05faf628e7c43df66f",
                //FilePath = @"c:\a.jpg"
                FilePath = filePath
            };

            if (!api.CreateTask())
            {
                res.Result = "API v2 send failed. " + api.ErrorMessage;
                res.Success = false;
            }
            else if (!api.WaitForResult())
            {
                res.Result = "Could not solve the captcha.";
                res.Success = false;
            }
            else
            {
                res.Result = api.GetTaskSolution().Text;
                res.Success = true;
            }
            return res;
        }
    }
}
