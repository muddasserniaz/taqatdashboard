﻿using CaptchaSolver.ApiResponse;
using Newtonsoft.Json.Linq;

namespace CaptchaSolver
{
    public interface IAnticaptchaTaskProtocol
    {
        JObject GetPostData();
        TaskResultResponse.SolutionData GetTaskSolution();
    }
}