﻿using System.Drawing;
using Syncfusion.OfficeChartToImageConverter;
using Syncfusion.Presentation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using CsvHelper;
using Syncfusion.PresentationToPdfConverter;
using Syncfusion.Pdf;

namespace TaqatDashboard

{
    public class TaqatDashboardPptWriter
    {
        
        public void dummayDashboard(Random rnd)
        {
            var resultListList = new List<List<TaqatDashboardResult>>();
            var resList1 = new List<TaqatDashboardResult>();

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.backend_user_login,
                PageLoadTime = GenRandInt(1500, 3000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.backend_user_url_availability,
                PageLoadTime = GenRandInt(1500, 3000, rnd)
            });
            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.employer_login,
                PageLoadTime = GenRandInt(4000, 11000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.employer_searching_candidate_profile,
                PageLoadTime = GenRandInt(1500, 6000, rnd)
            });
            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.employer_viewing_job_post_app_mgmt,
                PageLoadTime = GenRandInt(3000, 7000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.employer_viewing_job_post_overview,
                PageLoadTime = GenRandInt(2500, 7000, rnd)
            });
            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.guest_ind_registration,
                PageLoadTime = GenRandInt(2000, 6000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.guest_pub_employer_registration,
                PageLoadTime = GenRandInt(3000, 8000, rnd)
            });
            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.guest_pvt_employer_registration,
                PageLoadTime = GenRandInt(2000, 6000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.guest_searching_candidate_profiles,
                PageLoadTime = GenRandInt(3000, 7000, rnd)
            });
            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.guest_searching_job_post,
                PageLoadTime = GenRandInt(3000, 6000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.ind_user_login,
                PageLoadTime = GenRandInt(4000, 9000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.ind_user_searching_job_post,
                PageLoadTime = GenRandInt(2000, 6000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.ind_user_view_complaints,
                PageLoadTime = GenRandInt(2000, 7000, rnd)
            });
            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.ind_user_view_job_app,
                PageLoadTime = GenRandInt(2000, 7000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.ind_user_view_profile_info,
                PageLoadTime = GenRandInt(2000, 6000, rnd)
            });

            resList1.Add(new TaqatDashboardResult
            {
                MetricName = TaqatDashboardResult.DashbordMetrics.ind_user_view_program_obligation,
                PageLoadTime = GenRandInt(1500, 7500, rnd)
            });

            resultListList.Add(resList1);

            var x = new TaqatDashboardPptWriter();
            x.PrepareDashboard(resultListList);

            //Console.WriteLine("====================================");
            //Console.ReadLine();
        }

        public int GenRandInt(int one, int two, Random rnd)
        {
            var x = rnd.Next(one, two);
            return x;
        }

        public string PrepareDashboard(List<List<TaqatDashboardResult>> dashboardResultListList,string dashboardDateTime = "", string speedTest="")
        {
            //Opens an existing Presentation from the file system
            IPresentation presentation = Presentation.Open(@"c:\taqatdashboard\inputs\Dashboard - Mobile2.6 - Template - Upd6.pptx");

            var slide = presentation.Slides[0];

            //var FileNamePpt = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("DashboardReport_{0}.pptx", DateTime.Now.ToString("yyyy-dd-M--HH-mm"))); 
            //var FileNameJpg = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("DashboardReportImage_{0}.jpg", DateTime.Now.ToString("yyyy-dd-M--HH-mm")));

            var FileNamePpt = string.Format(@"c:\taqatdashboard\outputs\DashboardReport_{0}.pptx", DateTime.Now.ToString("yyyy-MMM-dd--HH-mm"));
            var FileNamePdf = string.Format(@"c:\taqatdashboard\outputs\DashboardReport_{0}.pdf", DateTime.Now.ToString("yyyy-MMM-dd--HH-mm"));
            var FileNameJpg = string.Format(@"c:\taqatdashboard\outputs\DashboardReportImage_{0}.jpg", DateTime.Now.ToString("yyyy-MMM-dd--HH-mm"));

            //add dashboard date time
            if (dashboardDateTime == "") {
                dashboardDateTime = DateTime.Now.ToString("dd MMM yyyy - hh tt");
            }
            var txtDateTimeShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "txt_datetime").FirstOrDefault();
            txtDateTimeShape.TextBody.Paragraphs[0].TextParts[0].Text = dashboardDateTime;// "21 Jul 18 4 PM";

            if (speedTest == "") speedTest = "10 ";
            var txtSpeedTestResultShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "txtSpeedTestResult").FirstOrDefault();
            txtSpeedTestResultShape.TextBody.Paragraphs[0].TextParts[0].Text = speedTest;// "21 Jul 18 4 PM";

            IColor thresholdColor;
            var source = 0;
            foreach (var dashboardResultList in dashboardResultListList)
            {
                foreach (var result in dashboardResultList)
                {
                    //find corresponding plt shape in file
                    foreach (IShape shp in presentation.Slides[0].Shapes)
                    {
                        if (shp.ShapeName == result.MetricName.ToString() + string.Format("_plt"))
                        {
                            try
                            {
                                shp.TextBody.Paragraphs[0].TextParts[0].Text = Math.Round(result.PageLoadTime / 1, 1).ToString();
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("shape_plt pptx merge error: {0},{1}\r\n{2}", shp.ShapeName,  result.MetricName.ToString(), ex.Message);
                            }
                        }
                    }

                    //find corresponding color shape in file
                    foreach (IShape shp in presentation.Slides[0].Shapes)
                    {
                        if (shp.ShapeName == result.MetricName.ToString() + string.Format("_color"))
                        {
                            if (result.PageLoadTime <= 5)
                            {
                                thresholdColor = ColorObject.DarkGreen;
                            }
                            else if
                                (result.PageLoadTime > 5 && result.PageLoadTime <= 10)
                            {
                                thresholdColor = ColorObject.LimeGreen;
                            }
                            else if
                                (result.PageLoadTime > 10 && result.PageLoadTime <= 20)
                            {
                                thresholdColor = ColorObject.Orange;
                            }
                            else
                            {
                                thresholdColor = ColorObject.Red;
                            }
                            shp.Fill.SolidFill.Color = thresholdColor;
                            //shp.TextBody.Paragraphs[0].TextParts[0].Text = result.PageLoadTime.ToString();
                        }
                    }

                    //find corresponding result shape in file
                    foreach (IShape shp in presentation.Slides[0].Shapes)
                    {
                        if (shp.ShapeName == result.MetricName.ToString() + string.Format("_result"))
                        {
                            try
                            {
                                shp.TextBody.Paragraphs[0].TextParts[0].Text = result.Result;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("shape_result pptx merge error: {0},{1}\r\n{2}", shp.ShapeName, result.MetricName.ToString(), ex.Message);
                            }
                        }
                    }

                    //var x  = presentation.Slides[0].Shapes.Where(_ => _.ShapeName.Contains(result.MetricName.)).FirstOrDefault();

                }
                source += 1; // next source results
            }

            //Saves the Presentation
            presentation.Save(FileNamePpt);

            //export as image
            ExportAsImage(presentation, FileNameJpg);

            //Saves the PDF document
            var pdfSettings = new PresentationToPdfConverterSettings();
            pdfSettings.ImageQuality = 30;
            PdfDocument pdfDocument = PresentationToPdfConverter.Convert(presentation, pdfSettings);

            pdfDocument.Save(FileNamePdf);

            //Closes the Presentation
            presentation.Close();

            return FileNamePdf;
        }

        public string PrepareDailyReport(
            string reportDate,
            string NoOfLoginsIndividual,
            string NoOfLoginsEmployer,
            string NoOfRegistrationsIndividual,
            string NoOfRegistrationsEmployer,
            string NoOfJobsPosted,
            string NoOfApplicattionsSubmitted,
            string NoOfMaxConnections
            )
        {

            //Opens an existing Presentation from the file system
            var presentation = Presentation.Open(@"c:\taqatdashboard\inputs\NLG Dashboard - Daily Report - Upd1 - Template.pptx");
            var slide = presentation.Slides[0];

            var txtDateShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "txt_date").FirstOrDefault();
            txtDateShape.TextBody.Paragraphs[0].TextParts[0].Text = reportDate;

            var txtNoOfLoginsIndividualShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_logins_individual").FirstOrDefault();
            txtNoOfLoginsIndividualShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfLoginsIndividual.ToString() ;

            var txtNoOfLoginsEmployerShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_logins_employer").FirstOrDefault();
            txtNoOfLoginsEmployerShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfLoginsEmployer.ToString();

            var txtNoOfRegistrationsIndividualShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_registrations_individual").FirstOrDefault();
            txtNoOfRegistrationsIndividualShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfRegistrationsIndividual.ToString();

            var txtNoOfRegistrationsEmployerShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_registrations_employer").FirstOrDefault();
            txtNoOfRegistrationsEmployerShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfRegistrationsEmployer.ToString();

            var txtNoOfJobsPostedShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_jobs_posted").FirstOrDefault();
            txtNoOfJobsPostedShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfJobsPosted.ToString();

            var txtNoOfApplicattionsSubmittedShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_applications_posted").FirstOrDefault();
            txtNoOfApplicattionsSubmittedShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfApplicattionsSubmitted.ToString();

            var txtNoOfMaxConnectionsShape = (IShape)presentation.Slides[0].Shapes.Where(_ => _.ShapeName == "no_of_max_connections").FirstOrDefault();
            txtNoOfMaxConnectionsShape.TextBody.Paragraphs[0].TextParts[0].Text = NoOfMaxConnections.ToString();

            var FileNamePpt = String.Format(@"c:\taqatdashboard\outputs\TaqatDailyReport_{0}.pptx", reportDate);
            var FileNamePdf = String.Format(@"c:\taqatdashboard\outputs\TaqatDailyReport_{0}.pdf", reportDate);

            //Saves the Presentation
            presentation.Save(FileNamePpt);
            var pdfSettings = new PresentationToPdfConverterSettings();
            pdfSettings.ImageQuality = 30;
            PdfDocument pdfDocument = PresentationToPdfConverter.Convert(presentation, pdfSettings);
            pdfDocument.Save(FileNamePdf);

            //Closes the Presentation
            presentation.Close();

            return FileNamePdf;
        }

        private void ExportAsImage(IPresentation presentation, string fileNameJpg)
        {

            //Declare variables to hold custom width and height
            int customWidth = 570 * 2;
            int customHeight = 900 * 2;

            //Converts the slide as image and returns the image stream
            Stream stream = presentation.Slides[0].ConvertToImage(Syncfusion.Drawing.ImageFormat.Emf);

            //Creates a bitmap of specific width and height
            Bitmap bitmap = new Bitmap(customWidth, customHeight, PixelFormat.Format32bppPArgb);

            //Gets the graphics from image
            Graphics graphics = Graphics.FromImage(bitmap);

            //Sets the resolution
            bitmap.SetResolution(graphics.DpiX, graphics.DpiY);

            //Recreates the image in custom size
            graphics.DrawImage(System.Drawing.Image.FromStream(stream), new Rectangle(0, 0, bitmap.Width, bitmap.Height));

            //Saves the image as bitmap 
            bitmap.Save(fileNameJpg);

        }

        public string ConvertCSVToReportAndSend(string filePath, string dashboardDateTime, string speedTest)
        {
            var dashboardResultListList = new List<List<TaqatDashboardResult>>();
            dashboardResultListList.Add(ReadInCSV(filePath));
            return PrepareDashboard(dashboardResultListList, dashboardDateTime, speedTest);
        }

        public void MergeSourceReports()
        {
            var file0 = @"D:\workdev\seleniumTest\seleniumTest\bin\Debug\outputs\file0.csv";
            var file1 = @"D:\workdev\seleniumTest\seleniumTest\bin\Debug\outputs\file1.csv";
            var file2 = @"D:\workdev\seleniumTest\seleniumTest\bin\Debug\outputs\file2.csv";
            var dashboardResultListList = new List<List<TaqatDashboardResult>>();
            dashboardResultListList.Add(ReadInCSV(file0));
            dashboardResultListList.Add(ReadInCSV(file1));
            dashboardResultListList.Add(ReadInCSV(file2));
            PrepareDashboard(dashboardResultListList);
        }

        public List<TaqatDashboardResult> ReadInCSV(string filePath)
        {
            List<TaqatDashboardResult> result;

            using (TextReader fileReader = File.OpenText(filePath))
            {
                var csv = new CsvReader(fileReader);
                csv.Configuration.MissingFieldFound = null;
                //result = csv.GetRecords<TaqatDashboardResult>();
                result = csv.GetRecords<TaqatDashboardResult>().ToList();
            }
            return result;
        }

        //public void test(List<TaqatDashboardResult> dashboardResultList, int source=0)
        //{
        //    //Opens an existing Presentation from the file system
        //    IPresentation presentation = Presentation.Open(@"Dashboard - Mobile2.6 - Template.pptx");
        //    var slide = presentation.Slides[0];
        //    //find corresponding plt shape in file
        //    foreach (var result in dashboardResultList)
        //    {
        //        //var group = presentation.Slides[0].GroupShapes.Where(_ => _.ShapeName == (int)result.MetricName);
        //        foreach (var shp in presentation.Slides[0].GroupShapes)
        //        {
        //            if (shp.ShapeName == result.MetricName.ToString() ) {
        //                shp.Shapes[0]. .TextBody.Paragraphs[0].TextParts[0].Text

        //            }

        //        //if (shp.ShapeName == result.MetricName.ToString() + "_plt")
        //        //{
        //        //    //Console.WriteLine("plt shape name {0} found for result {1}: ", shp.ShapeName, result.MetricName.ToString());
        //        //    try
        //        //    {
        //        //        shp.TextBody.Paragraphs[0].TextParts[0].Text = Math.Round(result.PageLoadTime / 1000, 1).ToString();
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Console.WriteLine(ex.Message);
        //        //    }
        //        //}
        //        }
        //    }
        //}
    }
}
