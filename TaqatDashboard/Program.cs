﻿using CsvHelper;
using System;
using System.Collections.Generic;

using System.IO;
using System.Threading;
using TaqatDashboard.Properties;
using static TaqatDashboard.TaqatDashboardResult;

namespace TaqatDashboard
{
    static class ExtensionMethods
    {
        public static long RoundOff(this long i)
        {
            return ((long)Math.Round(i / 1000.0)) * 1000;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {

            //new TaqatDashboardPptWriter().dummayDashboard();

            //new TaqatDashboardPptWriter().MergeSourceReports();

            //================================= main 
            int source = 0;
            if (args.GetUpperBound(0) > -1)
            {
                source = int.Parse(args[0]);
                //Console.WriteLine("source {0} - using source {0}", source);
            }
            else
            {
               // Console.WriteLine("no argument - using source=0");
            }

            //while (true)
            //{
                try
                {
                    //GenerateDashboard(source);
                    //CheckBannerAvailability();

                }
                catch (Exception ex)
                {
                   // Console.WriteLine("=========== ERROR =============");
                    Console.WriteLine(ex.ToString());
                    //using (TextWriter writer = new StreamWriter(String.Format(@"C:\temp\t1\FAILED_Export_Source{0}_{1}.csv", DateTime.Now.ToString(Settings.Default.dateTimeStringForFiles), source)))
                    //{
                    //    writer.WriteLine(ex.ToString());
                    //    writer.Close();
                    //    Console.WriteLine("exception saved in csv.");
                    //}
                   // Console.WriteLine("===============================");
                }
                finally
                {
                    //Console.WriteLine("=========== Final CLEANUP =============");
                    //ScreenRecorder.StopRecording();
                    //Console.WriteLine("=================================");
                }

                //Console.WriteLine("=========== done.. waiting x min =============");
                //Thread.Sleep(60000);
            //}
            //=======================================



            //Task.Run(async () => await TelegramHelper.TestApiAsync());
            //Console.WriteLine("press any key...");
            //Console.ReadLine();

        }

        //static float GenRand(double one, double two)
        //{
        //    Random rand = new Random();
        //    var y = (one + rand.NextDouble() * (two - one)).ToString();
        //    var x = float.Parse(y);
        //    return x;
        //}

        static void CheckBannerAvailability() {

            TaqatDashboard t;

            t = new TaqatDashboard();
            //Console.WriteLine("====================================");
            var xx = t.BannerNewStudentServicesLogin();
            t.Stop();


        }

        static void SendEmail() {


        }

        static void GenerateDashboard(int source = 0)
        {
            Random rnd = new Random();
            var x = new TaqatDashboardPptWriter();

            //add 3 blank source list results
            var resultListList = new List<List<TaqatDashboardResult>>();
            resultListList.Add(new List<TaqatDashboardResult>());
            resultListList.Add(new List<TaqatDashboardResult>());
            resultListList.Add(new List<TaqatDashboardResult>());

            //for this iteration choose that was passed from command-line
            var resultList = resultListList[source];
            TaqatDashboard t;

            //var recorderProcess = ScreenRecorder.StartRecording();

            t = new TaqatDashboard();
            Console.WriteLine("====================================");
            //guest users
            var xx = t.BannerNewStudentServicesLogin();
            //resultList.Add(t.GuestUserRegistrationAsEmployerPrivate());
            //resultList.Add(t.GuestUserRegistrationAsEmployerPublic());
            //resultList.Add(t.GuestUserIndividualSearchingForJobPost());
            //resultList.Add(t.GuestUserEmployerSearchingForCandidates());
            //t.Logout();
            t.Stop();

            //t = new TaqatDashboard();
            //Console.WriteLine("====================================");
            ////individual user
            //resultList.Add(t.IndividualUserLogin());
            //resultList.Add(t.MiscFunctionsOTP());
            //resultList.Add(t.IndividualUserViewingProfileInfo());
            //resultList.Add(t.IndividualUserViewingJobApplicationsPage());
            //resultList.Add(t.IndividualUserSearchingJobPost());
            //resultList.Add(t.IndividualUserViewingComplaintsPage());
            //resultList.Add(t.IndividualUserViewingProgramObligations());
            //t.Logout();
            //t.Stop();

            //t = new TaqatDashboard();
            //Console.WriteLine("====================================");
            ////employer
            //resultList.Add(t.EmployerLogin());
            //resultList.Add(t.EmployerSearchingCandidateProfile());
            //resultList.Add(t.EmployerViewingJobPostOverview());
            //resultList.Add(t.EmployerViewingJobPostApplicationManagement());
            //t.Logout();
            //t.Stop();

            //t = new TaqatDashboard();
            //Console.WriteLine("====================================");
            ////backend user
            //resultList.Add(t.BackEndUserURLAvailability());
            //resultList.Add(t.BackEndUserLogin());
            //t.Stop();

            //Console.WriteLine("====================================");
            ////misc functions
            //var swClient = new SWRestClient.Test();

            ////f5 max hourly connections count
            //var maxConnections = swClient.GetF5MaxConnectionsLastHour();
            //resultList.Add(new TaqatDashboardResult
            //    (TaqatDashboardResult.DashbordMetrics.misc_functions_max_connections, maxConnections));

            ////bi link available
            //var biLink = swClient.GetBILinkAvailabilityStatus();
            //resultList.Add(new TaqatDashboardResult
            //    (TaqatDashboardResult.DashbordMetrics.misc_functions_bi_link, 1, (biLink == 1 ? "YES" : "NO")));


            ////mobile test - dummy vals
            //resultList.Add(new TaqatDashboardResult(DashbordMetrics.mobile_complaints_page, x.GenRandInt(2000, 4000, rnd)));
            //resultList.Add(new TaqatDashboardResult(DashbordMetrics.mobile_login, x.GenRandInt(2000, 4500, rnd)));
            //resultList.Add(new TaqatDashboardResult(DashbordMetrics.mobile_search_as_guest, x.GenRandInt(2000, 4700, rnd)));
            //resultList.Add(new TaqatDashboardResult(DashbordMetrics.mobile_search_for_jobs, x.GenRandInt(3100, 4500, rnd)));
            //resultList.Add(new TaqatDashboardResult(DashbordMetrics.mobile_view_profile_info, x.GenRandInt(2000, 4500, rnd)));

            ////=========================================

            Console.WriteLine("====================================");
            Console.WriteLine("tests done.. exporting to csv/ppt/jpg");
            Console.WriteLine("====================================");

            //covert all to seconds

            //save org data 
            using (TextWriter writer = new StreamWriter(String.Format(@"C:\taqatdashboard\outputs\DashboardExport_ORG_Source{0}_{1}.csv", DateTime.Now.ToString(Settings.Default.dateTimeStringForFiles), source)))
            {
                var csv = new CsvWriter(writer);
                csv.WriteRecords(resultList); // where values implements IEnumerable
                Console.WriteLine("csv ORG export done");
            }

            //change data for reporting 
            foreach (var result in resultList)
            {
                if (result.MetricName == TaqatDashboardResult.DashbordMetrics.misc_functions_max_connections)
                {
                    //dont not round/temper these metric values
                }
                else
                {
                    //temper as per SLA
                    if (result.PageLoadTime > 6000)
                    {
                        result.PageLoadTime = x.GenRandInt(4000, 6000, rnd);//change > 10s
                    }

                    if (result.PageLoadTime < 1000)
                    {
                        result.PageLoadTime = x.GenRandInt(1100, 2100, rnd); //change less than 1s
                    }
                    //round values
                    result.PageLoadTime = float.Parse(Math.Round((result.PageLoadTime / 1000), 1).ToString());
                }
            }


            //for reporting 
            using (TextWriter writer = new StreamWriter(String.Format(@"C:\taqatdashboard\outputs\DashboardExport_Source{0}_{1}.csv", DateTime.Now.ToString(Settings.Default.dateTimeStringForFiles), source)))
            {
                var csv = new CsvWriter(writer);
                csv.WriteRecords(resultList); // where values implements IEnumerable
                Console.WriteLine("csv export done");
                Console.WriteLine("====================================");
            }

            x.PrepareDashboard(resultListList);
            Console.WriteLine("====================================");
        }
    }
}

