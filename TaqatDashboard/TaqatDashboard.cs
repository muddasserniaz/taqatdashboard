﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TaqatDashboard.Properties;

namespace TaqatDashboard
{
    public class TaqatDashboard
    {
        //ChromeDriver driver;
        RemoteWebDriver driver;
        Stopwatch stopwatch;
        //WebDriverWait wait;
        ChromeOptions chromeDriverOptions;

        public TaqatDashboard()
        {
            chromeDriverOptions = new ChromeOptions();
            //chromeDriverOptions.AddArgument("log-level=3");
            chromeDriverOptions.AddArguments("disable-infobars");

            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.SuppressInitialDiagnosticInformation = true;

            driver = new ChromeDriver(service, chromeDriverOptions, TimeSpan.FromSeconds(180));
            driver.Manage().Window.Maximize();
            //wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));up

            //driver = new FirefoxDriver();
            stopwatch = Stopwatch.StartNew();
        }

        public void Stop()
        {
            driver.Close();
            driver.Dispose();
        }

        ~TaqatDashboard()
        {
            //driver.Close();
            driver.Dispose();
            Console.WriteLine("~TaqatDashboard called...");
        }

        #region "banner-new urls test"

        public bool o365Login()
        {

            driver.Navigate().GoToUrl("https://portal.office365.com");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));

            //var popup = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#agree_button")));
            //popup.Click();

            //var txtUserName = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#user_id"))); //proceed link in tablular form

            //driver.FindElementByCssSelector("input#user_id").SendKeys("monitor");
            //driver.FindElementByCssSelector("input#password").SendKeys("Pnu@12345");

            //stopwatch.Restart();
            ////click
            //var clickButton = driver.FindElementByCssSelector("input#entry-login");
            //clickButton.Click();

            //driver.Navigate().GoToUrl("https://lms.pnu.edu.sa/webapps/portal/admin/preview_module.jsp?module_id=_153_1");

            //var pageloaded = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("table"))); //proceed link in tablular form

            ////Console.WriteLine("student services - login - time: {0}", stopwatch.ElapsedMilliseconds);

            //var lastMinute = driver.FindElementByCssSelector("table tbody tr:nth-child(2) td:nth-child(2)");
          
 
            return true; 
        }

        public bool BannerNewStudentServicesLogin()
        {

            driver.Navigate().GoToUrl("https://lms.pnu.edu.sa/webapps/portal/admin/preview_module.jsp?module_id=_153_1");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));

            var popup = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#agree_button")));
            popup.Click();

            var txtUserName = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#user_id"))); //proceed link in tablular form

            driver.FindElementByCssSelector("input#user_id").SendKeys("monitor");
            driver.FindElementByCssSelector("input#password").SendKeys("Pnu@12345");

            stopwatch.Restart();
            //click
            var clickButton = driver.FindElementByCssSelector("input#entry-login");
            clickButton.Click();

            driver.Navigate().GoToUrl("https://lms.pnu.edu.sa/webapps/portal/admin/preview_module.jsp?module_id=_153_1");

            var pageloaded = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("table"))); //proceed link in tablular form

            //Console.WriteLine("student services - login - time: {0}", stopwatch.ElapsedMilliseconds);

            var lastMinute = driver.FindElementByCssSelector("table tbody tr:nth-child(2) td:nth-child(2)");
            var last5Minute = driver.FindElementByCssSelector("table tbody tr:nth-child(3) td:nth-child(2)");
            var last30Minute = driver.FindElementByCssSelector("table tbody tr:nth-child(4) td:nth-child(2)");
            var last60Minute = driver.FindElementByCssSelector("table tbody tr:nth-child(5) td:nth-child(2)");
            var last180Minute = driver.FindElementByCssSelector("table tbody tr:nth-child(6) td:nth-child(2)");

            //// Assumes connectionString is a valid connection string.  
            //var constr = @"Server=172.17.88.22;Database=SolarwindsOrion;User ID=sa;Password=Pass@word1";

            //using (SqlConnection connection = new SqlConnection(constr))
            //{
            //    connection.Open();
            //    SqlCommand insertCommand = new SqlCommand("INSERT INTO _PNULMSCurrentUsers (lastMinute, last5Minute, last30Minute, last60Minute, last180Minute) VALUES (@lastMinute, @last5Minute, @last30Minute, @last60Minute, @last180Minute)", connection);
            //    insertCommand.Parameters.Add(new SqlParameter("lastMinute", lastMinute.Text));
            //    insertCommand.Parameters.Add(new SqlParameter("last5Minute", last5Minute.Text));
            //    insertCommand.Parameters.Add(new SqlParameter("last30Minute", last30Minute.Text));
            //    insertCommand.Parameters.Add(new SqlParameter("last60Minute", last60Minute.Text));
            //    insertCommand.Parameters.Add(new SqlParameter("last180Minute", last180Minute.Text));
            //    insertCommand.ExecuteNonQuery();
            //}


            //Console.WriteLine("Statistic.A:10");

            Console.WriteLine("Statistic.A:{0}", lastMinute.Text);
            Console.WriteLine("Statistic.B:{0}", last5Minute.Text);
            Console.WriteLine("Statistic.C:{0}", last30Minute.Text);
            Console.WriteLine("Statistic.D:{0}", last60Minute.Text);
            Console.WriteLine("Statistic.E:{0}", last180Minute.Text);

            return true; //new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.backend_user_login, stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region "misc"
        public TaqatDashboardResult MiscFunctionsOTP()
        {
            stopwatch.Restart();

            driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/useraccountdetails?p_p_id=IndAccountDetailsportlet_WAR_IndAccountDetailsportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=updateMobileNumberRequest&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_count=1&contactNo=+966551500439");
            Console.WriteLine("OTP sent...waiting for receipt...");
            //try to get response for OPT receipt, wait 5min before quiting, try every second
            bool res = false;
            while (res == false && stopwatch.ElapsedMilliseconds < (600 * 10))
            {
                res = ReadOTPResponse();
                Console.WriteLine("res : {0}", res);
                if (res == false)
                {
                    Console.WriteLine("no response found .. trying again.. time(ms) in loop {0}, will wait 5 min", stopwatch.ElapsedMilliseconds);
                    Thread.Sleep(1000); //wait 1 sec before retry
                }
            }

            //driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/useraccountdetails");

            ////wait for next page to load
            //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            ////var newPageIsLoaded = wait.Until(ExpectedConditions.UrlContains("web/individual/useraccountdetails"));
            //var changeMobileNoButton = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#sendOTP")));// verify that changeMobileNo is clickable
            //changeMobileNoButton.Click();

            //var oTPTextbox = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#_IndAccountDetailsportlet_WAR_IndAccountDetailsportlet_mobileNumber")));// verify that changeMobileNo is clickable
            //oTPTextbox.Click();
            //oTPTextbox.SendKeys("551500439");

            //driver.FindElementByCssSelector("#sendOTP").Click();
            Console.WriteLine("ind user - otp receiving - time: {0}", stopwatch.ElapsedMilliseconds.RoundOff());
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.misc_functions_otp, stopwatch.ElapsedMilliseconds);

        }


        private bool ReadOTPResponse()
        {
            Console.WriteLine("in read_to_presponse...");
            HttpWebRequest request = WebRequest.Create("http://52.55.149.22/default?OPTReceivedGet=1") as HttpWebRequest;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            WebHeaderCollection header = response.Headers;
            var encoding = ASCIIEncoding.ASCII;
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
            {
                string responseText = reader.ReadToEnd();
                if (responseText == "1")
                {
                    Console.WriteLine("response received..returning true");
                    return true;
                }

                else
                {
                    Console.WriteLine("response received..returning false");
                    return false;
                }
            }

        }

        #endregion


        #region "backend login"
        public TaqatDashboardResult BackEndUserURLAvailability()
        {
            stopwatch.Restart();
            driver.Navigate().GoToUrl("https://taqat.sa/web/guest/backendnlglogin");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var txtUserName = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#username"))); //proceed link in tablular form

            Console.WriteLine("backend user - url availability - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.backend_user_url_availability, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult BackEndUserLogin()
        {

            driver.Navigate().GoToUrl("https://taqat.sa/web/guest/backendnlglogin");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var txtUserName = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#username"))); //proceed link in tablular form

            driver.FindElementByCssSelector("input#username").SendKeys(Settings.Default.BackendUserName);
            driver.FindElementByCssSelector("input#password").SendKeys(Settings.Default.BackendPassword);

            stopwatch.Restart();
            //click
            driver.FindElementById("ctl00_ContentPlaceHolder1_wucLoginForBackoffice_btnLogin").Click();

            var pageloaded = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#_AccessFunctionalityportlet_WAR_AccessFunctionalityportlet_role_section_chosen_labeloffield"))); //proceed link in tablular form

            Console.WriteLine("backend user - login - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.backend_user_login, stopwatch.ElapsedMilliseconds);
        }
        #endregion

        #region "individual users"

        public TaqatDashboardResult IndividualUserLogin()
        {

            var captchaUnsolved = true; //captcha not solved yet
            var captchaSolveTries = 1;

            while (captchaUnsolved && captchaSolveTries < 3) //loop until captcha is solved or tries exceed
            {
                GotoPageHome();
                EnterAsIndividual();
                GoToPageLoginIndividual();

                Console.WriteLine("solving captcha ...");
                var captchaFile = SaveCaptcha();
                var captchaResult = CaptchaSolver.Solver.SolveImageToText(captchaFile);
                if (captchaResult.Success)
                {

                    Console.WriteLine("captcha solved ...resuming...user/pass entering ");

                    driver.FindElementByCssSelector("input#txtUsername").SendKeys(Settings.Default.IndividualUserName);
                    driver.FindElementByCssSelector("input#txtPassword").SendKeys(Settings.Default.IndividualPassword);
                    driver.FindElementByCssSelector("input#CaptchaCodeTextBox").SendKeys(captchaResult.Result);

                    driver.FindElementByCssSelector("#ctl00_ContentPlaceHolder1_wucLoginForIndividual_btnLogin").Click();

                    try
                    {
                        stopwatch.Restart();
                        Console.WriteLine("waiting for ind home page to load... ");
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                        var userHomePagePanel = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst")));
                        captchaUnsolved = false; //captcha solved, reached user home page
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("login was not successful, user home page not loaded... trying again.. failed captcha details - image: {0}, result: {1} ", captchaFile, captchaResult);
                        captchaSolveTries += 1;
                    }
                }
                else
                {
                    Console.WriteLine("captcha could not be solved by anyone, trying again.. failed captcha details - image: {0}, result: {1} ", captchaFile, captchaResult);
                    captchaSolveTries += 1;
                }
            }
            Console.WriteLine("ind user - login - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.ind_user_login, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult IndividualUserViewingProfileInfo()
        {
            //TODO: perform actual interaction

            ////element refering to user menu
            //var rootMenu = driver.FindElement(By.CssSelector("a.userLogined"));

            //var action = new Actions(driver);
            //action.MoveToElement(rootMenu).Click().Perform();
            //Thread.Sleep(1000);
            //Console.WriteLine("clicked on user menu.. now going to click on personal details");
            //action.MoveToElement(driver.FindElement(By.XPath("//a[text()='Personal details']"))).Click().Perform();
            //driver.FindElement(By.XPath("//a[text()='Personal details']")).Click();

            stopwatch.Restart();
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/my-details");

            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

            //TODO: check DOM is ready then calc time
            var newPageIsLoaded = wait.Until(ExpectedConditions.UrlContains("web/individual/my-details"));

            Console.WriteLine("ind user - viewing profile - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.ind_user_view_profile_info, stopwatch.ElapsedMilliseconds);
        }


        public TaqatDashboardResult IndividualUserViewingJobApplicationsPage()
        {
            //TODO: change to mouse interaction to verify menu working
            stopwatch.Restart();
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/applications");

            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            //TODO: check some actual elememt, as sometimes there is not data and we consider it working
            var newPageIsLoaded = wait.Until(ExpectedConditions.UrlContains("web/individual/applications"));
            Console.WriteLine("ind user - viewing job application - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.ind_user_view_job_app, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult IndividualUserSearchingJobPost()
        {
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/home");
            stopwatch.Restart();
            driver.FindElementById("_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst").SendKeys("software");
            driver.FindElementByCssSelector("button#searchBut").Click();

            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var searchResultLabel = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("p#resultDiv div")));

            //var searchResultContainsJobs = searchResultLabel.Text.Contains("available");
            var searchResultJobCount = searchResultLabel.Text.Split()[0];
            //var isNumeric = Regex.IsMatch(searchResultJobCount, @"^\d+$"); // if result contains valid job results
            var isNumeric = searchResultJobCount.All(Char.IsNumber);
            //if (searchResultJobCount.GetType()== string)

            Console.WriteLine("ind user - searching job jost - time: {0}, results: {1} ", stopwatch.ElapsedMilliseconds, searchResultJobCount);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.ind_user_searching_job_post, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult IndividualUserViewingComplaintsPage()
        {
            stopwatch.Restart();
            //TODO: make it real scenario
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/complaints");

            //driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/home");
            ////wait for next page to load
            //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            //var complaintsLink = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("a[href*='complaints']")));
            //var actions = new Actions(driver); //scroll to element
            //actions.MoveToElement(complaintsLink);
            //actions.Perform();

            //stopwatch.Restart();
            ////var individualUserComplaintsPageLink = driver.FindElementByXPath("//a[text()='Complaints']");
            //var individualUserComplaintsPageLink = driver.FindElementByCssSelector("a[href*='complaints']");
            //individualUserComplaintsPageLink.Click();
            Console.WriteLine("ind user - viewing complaints - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.ind_user_view_complaints, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult IndividualUserViewingProgramObligations()
        {
            stopwatch.Restart();
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/individual/obligations");
            Console.WriteLine("ind user obligation page loaded - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.ind_user_view_program_obligation, stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region "employer"

        public TaqatDashboardResult EmployerLogin()
        {
            GotoPageHome();
            EnterAsEmployer();
            GoToPageLoginEmployer();

            stopwatch.Restart();

            driver.FindElementByCssSelector("input#txtUsername").SendKeys(Settings.Default.EmployerUserName);
            driver.FindElementByCssSelector("input#txtPassword").SendKeys(Settings.Default.EmployerPassword);

            //driver.FindElementByCssSelector("input[value='Login']").Click();
            driver.FindElementById("ctl00_ContentPlaceHolder1_wucLoginForPublicEmployer_btnLogin").Click();
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                //var employerCompanySelectLink = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("form[name*='selectCompanyForm'] a[onclick*='PROCEED']"))); //proceed link in tablular form
                //var employerCompanySelectLink = driver.FindElementByXPath("//a[text()='Proceed']");
                //employerCompanySelectLink.Click();
                var employerLandingPageSearchTextBox = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst"))); //search textbox, to confirm page is loaded

            }
            catch (Exception ex)
            {
                Console.WriteLine("Employer login was not successful: error {0}", ex.Message);
                //throw;
            }

            Console.WriteLine("employer login page loaded - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.employer_login, stopwatch.ElapsedMilliseconds);

        }

        public TaqatDashboardResult EmployerSearchingCandidateProfile()
        {

            driver.Navigate().GoToUrl("https://www.taqat.sa/web/employer/home"); //employer home page
            stopwatch.Restart();

            Console.WriteLine("employer searching candidates.. may take a lot of time.. ");
            //wait for loader to become invisible
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var loader = wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector("#loader-container")));

            driver.FindElementByCssSelector("#_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst").SendKeys("software");
            driver.FindElement(By.CssSelector("#searchBut")).Click();

            Console.WriteLine("search initiated... waiting for result page...");

            //wait for next page to load
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var searchResultLabel = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("span#total-result")));
            var searchResultCount = searchResultLabel.Text.Split()[0];
            var isNumeric = searchResultCount.All(Char.IsNumber);
            Console.WriteLine("employer - seaching candidates - time: {0}, search result count/success:  {1} ", stopwatch.ElapsedMilliseconds, searchResultCount);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.employer_searching_candidate_profile, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult EmployerViewingJobPostOverview()
        {
            stopwatch.Restart();
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/employer/job-post-overview");
            Console.WriteLine("employer - viewing job post overview - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.employer_viewing_job_post_overview, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult EmployerViewingJobPostApplicationManagement()
        {
            stopwatch.Restart();
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/employer/create-new-job-post");

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var xxx = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#continueBtn")));

            //var element = driver.FindElement(By.Id("_employerJobPost_WAR_employerJobPost10SNAPSHOT_jobPostType0"));
            //var actions = new Actions(driver);
            //actions.MoveToElement(element).Click().Perform();

            //driver.FindElementByCssSelector("#_employerJobPost_WAR_employerJobPost10SNAPSHOT_jobPostType0").SendKeys(Keys.Return);


            //TODO: make test on real condition, check time after selecting job time and then measuring on new page
            //driver.FindElementByCssSelector("label.css-radio-label").Click(); //select first job post type from radio button
            //driver.FindElementByCssSelector("#continueBtn").Click(); //click submit

            //var newJobPostFormOccupationTextbox = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#_employerJobPost_WAR_employerJobPost10SNAPSHOT_occupationsInput")));

            Console.WriteLine("employer - viewing job post application management - time:{0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.employer_viewing_job_post_app_mgmt, stopwatch.ElapsedMilliseconds);
        }


        #endregion

        #region "guest users"

        public TaqatDashboardResult GuestUserRegistrationAsIndividual()
        {
            GotoPageHome();
            EnterAsIndividual();

            stopwatch.Restart();
            //GoToPageNewRegistration(); //new reg as ind 
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/guest/individualregistration");
            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var registrationTypeButton = wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#_58_NSAUD")));

            Console.WriteLine("guest user - reg as new individual - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.guest_ind_registration, stopwatch.ElapsedMilliseconds);

        }

        public TaqatDashboardResult GuestUserRegistrationAsEmployerPrivate()
        {
            GotoPageHome();
            EnterAsEmployer();

            stopwatch.Restart();
            GoToPageNewRegistrationEmployer();

            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var registrationTypeButton = wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#_58_PRIV")));

            Console.WriteLine("guest user - reg as new employer private - time: {0}", stopwatch.ElapsedMilliseconds); //stop here, private employer is redirected to mol site, just measure load time and exit
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.guest_pvt_employer_registration, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult GuestUserRegistrationAsEmployerPublic()
        {
            GotoPageHome();
            EnterAsEmployer();

            stopwatch.Restart();
            GoToPageNewRegistrationEmployer();

            //TODO: replace it with actual test
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var registrationTypeButton = wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#_58_PUBE")));

            //driver.FindElementByCssSelector("#_58_PUBE").Click(); //click reg as public employer

            Console.WriteLine("guest user - reg as new employer - time: {0}", stopwatch.ElapsedMilliseconds);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.guest_pub_employer_registration, stopwatch.ElapsedMilliseconds);
        }


        public TaqatDashboardResult GuestUserIndividualSearchingForJobPost()
        {
            GotoPageHome();
            EnterAsIndividual();

            stopwatch.Restart();
            driver.FindElementById("_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst").SendKeys("software");
            driver.FindElementByCssSelector("button#searchBut").Click();

            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            var searchResultLabel = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("p#resultDiv div")));

            //var searchResultContainsJobs = searchResultLabel.Text.Contains("available");
            var searchResultJobCount = searchResultLabel.Text.Split()[0];
            //var isNumeric = Regex.IsMatch(searchResultJobCount, @"^\d+$"); // if result contains valid job results
            var isNumeric = searchResultJobCount.All(Char.IsNumber);
            //if (searchResultJobCount.GetType()== string)

            Console.WriteLine("guest user - ind searching job post - time: {0}, job search success:  {1} ", stopwatch.ElapsedMilliseconds, searchResultJobCount);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.guest_searching_job_post, stopwatch.ElapsedMilliseconds);
        }

        public TaqatDashboardResult GuestUserEmployerSearchingForCandidates()
        {
            GotoPageHome();
            EnterAsEmployer();

            stopwatch.Restart();
            driver.FindElementById("_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst").SendKeys("software");
            driver.FindElementByCssSelector("button#searchBut").Click();

            //wait for next page to load
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            var searchResultLabel = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("span#total-result")));

            //var searchResultContainsJobs = searchResultLabel.Text.Contains("available");
            var searchResultCount = searchResultLabel.Text.Split()[0];
            //var isNumeric = Regex.IsMatch(searchResultJobCount, @"^\d+$"); // if result contains valid job results
            var isNumeric = searchResultCount.All(Char.IsNumber);
            //if (searchResultJobCount.GetType()== string)

            Console.WriteLine("guest user - employer searching for candidates - time: {0}, results: {1}", stopwatch.ElapsedMilliseconds, searchResultCount);
            return new TaqatDashboardResult(TaqatDashboardResult.DashbordMetrics.guest_searching_candidate_profiles, stopwatch.ElapsedMilliseconds);

        }

        #endregion

        #region "Helpers"
        private void EnterAsIndividual()
        {
            //var enterAsIndividual = driver.FindElementByCssSelector("#selectedindividual");
            //enterAsIndividual.Click();

            driver.Navigate().GoToUrl("https://www.taqat.sa/web/guest/individual");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            var individualHomePageLoaded = wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#_matchAndSearch_WAR_matchAndSearch100SNAPSHOT_keywordFirst")));
            CloseNewDiag();
        }

        private void EnterAsEmployer()
        {

            //wait for next page to load, confirm that new page is loaded
            try
            {
                driver.Navigate().GoToUrl("https://www.taqat.sa/web/guestemployer/home");

                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
                var employerHomePageLoaded = wait.Until(ExpectedConditions.ElementExists(By.CssSelector("a[href*='create-new-job-post']"))); //search job post button is clickable
                CloseNewDiag();

            }
            catch (Exception ex)
            {
                Console.WriteLine("could not enter a employer, trying again. error was: {0}", ex.Message);
                driver.Manage().Cookies.DeleteAllCookies();
                driver.Close();
                driver = new ChromeDriver(chromeDriverOptions);
                driver.Navigate().GoToUrl("https://www.taqat.sa/");

                EnterAsEmployer(); // try me again
            }


        }
        private void CloseNewDiag()
        {
            var newDiag = driver.FindElementByCssSelector("button.ui-dialog-titlebar-close");
            if (newDiag != null)
            {
                newDiag.Click();
            }
        }
        private void GoToPageNewRegistration()
        {
            //click new user link, for registration as individual or as employer, link is diff but text is same for both
            //var newUserRegistrationLink = driver.FindElementByXPath("//a[text()='مستخدم جديد؟']");
            //var newUserRegistrationLink = driver.FindElementByXPath("//a[text()='New user?']");


            var newUserRegistrationLink = driver.FindElementByCssSelector("a[href*='registration']");

            newUserRegistrationLink.Click();


        }

        private void GoToPageNewRegistrationEmployer()
        {
            //click new user link, for registration as individual or as employer, link is diff but text is same for both
            //var newUserRegistrationLink = driver.FindElementByXPath("//a[text()='مستخدم جديد؟']");
            //var newUserRegistrationLink = driver.FindElementByXPath("//a[text()='New user?']");

            //var newUserRegistrationLink = driver.FindElementByCssSelector("a[href*='registration']");
            //newUserRegistrationLink.Click();
            Console.WriteLine("in GoToPageNewRegistrationEmployer");
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/guestemployer/employerregistration");

        }

        private void GoToPageLoginIndividual()
        {
            Console.WriteLine("clicking login link...");
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/guest/individuallogin");
            //click new user link, for registration as individual or as employer, link is diff but text is same for both
            //var loginLink = driver.FindElementByXPath("//a[text()='Log In']");
            //var loginLink = driver.FindElementByCssSelector("a[href*='login'] ");
            //loginLink.Click();
            Console.WriteLine("exiting .. GoToPageLogin...");

        }
        private void GoToPageLoginEmployer()
        {
            Console.WriteLine("clicking employer login link...");
            driver.Navigate().GoToUrl("https://www.taqat.sa/web/guestemployer/employerlogin");
            Console.WriteLine("exiting .. GoToPageLogin employer...");

        }

        public void Logout()
        {
            try
            {
                var LogoutLink = driver.FindElementByCssSelector("a.logout_link");
                LogoutLink.Click();
                Console.WriteLine("logging out...");
            }
            catch (Exception)
            {
                //Console.WriteLine("logout link not found.. exception handled");
            }
        }

        private string SaveCaptcha()
        {
            //var fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("captcha_{0}.jpg", DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss"))); ;
            var fileName = string.Format(@"C:\taqatdashboard\outputs\captcha_{0}.jpg", DateTime.Now.ToString(Settings.Default.dateTimeStringForFiles)); ;
            var base64string = driver.ExecuteScript(@"
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = document.getElementById('c_formssignin_examplecaptcha_CaptchaImage');
    c.height=img.height;
    c.width=img.width;
    ctx.drawImage(img, 0, 0,img.width, img.height);
    var base64String = c.toDataURL();
    return base64String;
    ") as string;

            var base64 = base64string.Split(',').Last();
            using (var stream = new MemoryStream(Convert.FromBase64String(base64)))
            {
                using (var bitmap = new Bitmap(stream))
                {
                    //var filepath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "captcha.jpg");
                    bitmap.Save(fileName, ImageFormat.Jpeg);
                }
            }

            return fileName;
        }

        private void GotoPageHome()
        {
            driver.Navigate().GoToUrl("https://www.taqat.sa/");
            //driver.Navigate().GoToUrl("https://172.24.100.12/");
            ChangeLanguageToEnglishIfNotAlready();
        }

        private void ChangeLanguageToEnglishIfNotAlready()
        {
            try
            {
                var langButton = driver.FindElementByCssSelector("div a.language-button");
                if (langButton.Text == "English") //if arabic lang then switch to engligh
                {
                    langButton.Click();
                    Console.WriteLine("lang changed by link1");
                }
            }
            catch (NoSuchElementException)
            {
                //Console.WriteLine("lang1 link failed.");
            }

            //2nd link that comes on employer login page
            try
            {
                var lang2Button = driver.FindElementByCssSelector("input#lang_button");
                if (lang2Button.Text == "English") //if arabic lang then switch to engligh
                {
                    lang2Button.Click();
                    Console.WriteLine("lang changed by link2");
                }
            }
            catch (NoSuchElementException)
            {
                //Console.WriteLine("lang2 link failed.");
            }

        }
        #endregion

    }


}
