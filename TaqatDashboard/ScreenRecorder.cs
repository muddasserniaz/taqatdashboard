﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace TaqatDashboard
{
    public static class ScreenRecorder
    {
        public static Process StartRecording()
        {
            var process = new System.Diagnostics.Process();
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\Program Files\Sharex\Sharex.exe";
            startInfo.Arguments = @"-StartScreenRecorder -AutoClose";

            process.StartInfo = startInfo;
            process.Start(); //start recording
            return process;
        }

        public static void StopRecording()
        {
            Console.WriteLine("stopping recording.. checking recorder running?");
            var process = Process.GetProcessesByName("sharex").FirstOrDefault();
            if (process != null)
            {
                //try closing normally
                Console.WriteLine("recorder was found running.. trying closing process normally ..");
                var startInfo = new ProcessStartInfo();
                startInfo.FileName = @"C:\Program Files\Sharex\Sharex.exe";
                startInfo.Arguments = @"-StartScreenRecorder -AutoClose";
                process.StartInfo = startInfo;
                process.Start();
                Thread.Sleep(3000);
                Console.WriteLine("after gracefull shutdown...status now: still running? {0}", Process.GetProcessesByName("sharex").Any());
                
                //check pross state
                if (Process.GetProcessesByName("sharex").Any())
                {
                    //if still not down
                    Console.WriteLine("recorder was found running..again.. killing it forcefully ...status now: still running?{0}", Process.GetProcessesByName("sharex").Any());
                    process.Kill();
                    Thread.Sleep(3000);
                    Console.WriteLine("after killing...status now: still running? {0}", Process.GetProcessesByName("sharex").Any());

                    //kill all ffmpeg processes
                    
                    var ffmpegProcesses = Process.GetProcessesByName("ffmpeg");
                    Console.WriteLine("checking ffmpeg process count {0}", ffmpegProcesses.Count());
                    if (ffmpegProcesses.Count() > 0)
                        ffmpegProcesses.ToList().ForEach(p => 
                            p.Kill()
                        );
                    ffmpegProcesses = Process.GetProcessesByName("ffmpeg");
                    Console.WriteLine("after kill checking ffmpeg process count {0}", ffmpegProcesses.Count());
                    

                }
            }
        }

        
    }
}
