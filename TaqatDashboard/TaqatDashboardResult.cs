﻿using Syncfusion.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaqatDashboard
{
    public class TaqatDashboardResult
    {
        public DashbordMetrics MetricName { get; set; }
        public Single PageLoadTime { get; set; }
        public string Result { get; set; }
        public ColorObject ThresholdColor { get; private set; }

        public enum DashbordMetrics
        {

            ind_user_login = 30,
            ind_user_view_profile_info=79,
            ind_user_view_job_app=135,
            ind_user_searching_job_post=144,
            ind_user_view_complaints=151,
            ind_user_view_program_obligation=166,

            backend_user_url_availability=174,
            backend_user_login=183,

            employer_login=192,
            employer_searching_candidate_profile=290,
            employer_viewing_job_post_overview=223,
            employer_viewing_job_post_app_mgmt=251,

            guest_ind_registration=261,
            guest_pvt_employer_registration=269,
            guest_pub_employer_registration=277,
            guest_searching_job_post=286,
            guest_searching_candidate_profiles=300,
            misc_functions_otp, 
            misc_functions_max_connections,
            misc_functions_bi_link,

            mobile_search_as_guest,
            mobile_login,
            mobile_view_profile_info,
            mobile_search_for_jobs,
            mobile_complaints_page

        }

        public TaqatDashboardResult(DashbordMetrics metricName, Single pageLoadTime, string result ="")
        {
            MetricName = metricName;
            PageLoadTime = pageLoadTime;
            Result = result;
        }

        public TaqatDashboardResult() { }

        private void SetThresholdColor()
        {

        }

    }

}
